package com.example.back.model;

import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "exit_movement")
public class ExitMovement {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_create")
    private String userCreate;

    @Column(name = "date_movement")
    private Date date_movement;


    @Column(name = "warehouse_Name")
    private String warehouseName;

    @Column(name = "warehouse_Code")
    private String warehouseCode;

    @Column(name = "officer_Type")
    private String officerType;

    @Column(name = "ref_Type")
    private String refType;

    @Column(name = "reference")
    private Long reference;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "total_Quantity")
    private Integer totalQuantity;

    @Column(name = "total_Weight")
    private Integer totalWeight;

    @Column(name = "merchandise_Description")
    private String merchandiseDescription;

    @Column(name = "document_Type")
    private String documentType;

    @Column(name = "document_Reference")
    private Long documentReference;
}
