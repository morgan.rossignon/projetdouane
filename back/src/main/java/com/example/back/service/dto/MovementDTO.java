package com.example.back.service.dto;

import java.util.Date;
import lombok.Data;

@Data
public class MovementDTO {

    private long id;
    private String userCreate;
    private Date date_movement;
    private String warehouseName;
    private String warehouseCode;
    private String officerType;
    private String refType;
    private Long reference;
    private Integer quantity;
    private Integer weight;
    private Integer totalQuantity;
    private Integer totalWeight;
    private String merchandiseDescription;
}
