package com.example.back.service.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ExitMovementDTO extends MovementDTO {
    private String documentType;
    private Long documentReference;
}
