package com.example.back.service;

import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.back.mapper.EntryMovementMapper;
import com.example.back.repository.EntryMovementRepository;
import com.example.back.service.dto.EntryMovementDTO;
import com.example.back.service.email.EmailService;

@Service
public class EntryMovementService {

    private final EntryMovementMapper entryMovementMapper = Mappers.getMapper(EntryMovementMapper.class);
    private final EntryMovementRepository entryMovementRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    public EntryMovementService(EntryMovementRepository entryMovementRepository) {
        this.entryMovementRepository = entryMovementRepository;
    }

    public List<EntryMovementDTO> findAll() {
        return entryMovementMapper.mapListToDTO(entryMovementRepository.findAll());
    }

    public void save(EntryMovementDTO entryMovementDTO) throws Exception {
        if ("AWB".equals(entryMovementDTO.getRefType()) && entryMovementDTO.getReference().toString().length() != 11) {
            throw new Exception("La référence doit contenir exactement 11 chiffres si AWB.");
        }else if(entryMovementDTO.getTotalQuantity() < entryMovementDTO.getQuantity()){
            throw new Exception("La quantité totale doit être supérieure ou égale à la quantité de la marchandise du mouvement.");
        }else if(entryMovementDTO.getTotalWeight() < entryMovementDTO.getWeight()){
            throw new Exception("Le poids total doit être supérieur ou égale au poids de la marchandise du mouvement.");
        };
        entryMovementDTO.setUserCreate("createUser");
        entryMovementDTO.setDate_movement(new Date());
        this.sendEmail(entryMovementDTO);
        entryMovementRepository.save(entryMovementMapper.map(entryMovementDTO));
    }

    private void sendEmail(EntryMovementDTO entryMovementDTO) throws MessagingException {
        emailService.sendEmail("morgan.rossignon@gmail.com", "Mouvement effectué", "Voici un fichier XML récapitulatif du mouvement", entryMovementDTO);
    }
}
