package com.example.back.service;

import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.back.mapper.EntryMovementMapper;
import com.example.back.mapper.ExitMovementMapper;
import com.example.back.repository.EntryMovementRepository;
import com.example.back.repository.ExitMovementRepository;
import com.example.back.service.dto.EntryMovementDTO;
import com.example.back.service.dto.ExitMovementDTO;
import com.example.back.service.email.EmailService;

@Service
public class ExitMovementService {

    private final ExitMovementMapper exitMovementMapper = Mappers.getMapper(ExitMovementMapper.class);
    private final ExitMovementRepository exitMovementRepository;

    private final EntryMovementMapper entryMovementMapper = Mappers.getMapper(EntryMovementMapper.class);
    private final EntryMovementRepository entryMovementRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    public ExitMovementService(ExitMovementRepository exitMovementRepository, EntryMovementRepository entryMovementRepository) {
        this.exitMovementRepository = exitMovementRepository;
        this.entryMovementRepository = entryMovementRepository;
    }

    public List<ExitMovementDTO> findAll() {
        return exitMovementMapper.mapListToDTO(exitMovementRepository.findAll());
    }

    public void save(ExitMovementDTO exitMovementDto) throws Exception {
        if ("AWB".equals(exitMovementDto.getRefType()) && exitMovementDto.getReference().toString().length() != 11) {
            throw new Exception("La référence doit contenir exactement 11 chiffres si AWB.");
        }else if(exitMovementDto.getTotalQuantity() < exitMovementDto.getQuantity()){
            throw new Exception("La quantité totale doit être supérieure ou égale à la quantité de la marchandise du mouvement.");
        }else if(exitMovementDto.getTotalWeight() < exitMovementDto.getWeight()){
            throw new Exception("Le poids total doit être supérieur ou égale au poids de la marchandise du mouvement.");
        }else if(!this.exitReferenceIsEntry(exitMovementDto)){
            System.out.println("not exist reference entry");
            throw new Exception("La référence de sortie doit être une référence d'entrée.");
        };
        exitMovementDto.setUserCreate("createUser");
        exitMovementDto.setDate_movement(new Date());
        this.sendEmail(exitMovementDto);
        exitMovementRepository.save(exitMovementMapper.map(exitMovementDto));
    }

    private boolean exitReferenceIsEntry(ExitMovementDTO exitMovementDto){
        List<EntryMovementDTO> entries = entryMovementMapper.mapListToDTO(entryMovementRepository.findAll());
        for (EntryMovementDTO entry : entries) {
            if(entry.getReference().toString().equals(exitMovementDto.getReference().toString())){
                return true;
            };
        };
        return false;
    }

    private void sendEmail(ExitMovementDTO exitMovementDTO) throws MessagingException {
        emailService.sendEmail("morgan.rossignon@gmail.com", "Mouvement effectué", "Voici un fichier XML récapitulatif du mouvement", exitMovementDTO);
    }
}
