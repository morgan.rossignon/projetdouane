package com.example.back.service.email;

import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.jdom2.*;
import org.jdom2.output.*;

import com.example.back.service.dto.EntryMovementDTO;
import com.example.back.service.dto.ExitMovementDTO;
import com.example.back.service.dto.MovementDTO;


@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    private String pathFileXML = "./back/src/main/java/com/example/back/service/email/movement.xml";

    public void sendEmail(String to, String subject, String body, MovementDTO movementDTO) throws MessagingException {
        try {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(message, true);
            messageHelper.setTo(to);
            messageHelper.setSubject(subject);
            messageHelper.setText(body);
            createFileXML(movementDTO);
            FileSystemResource file = new FileSystemResource(new File(this.pathFileXML));
            messageHelper.addAttachment("movement.xml", file);
            javaMailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private void createFileXML(MovementDTO movementDTO) {
        try {//// attention diff entre in et out
            Element CargoMessage = new Element("CargoMessage");
            if(movementDTO instanceof EntryMovementDTO) {
                CargoMessage.setAttribute("type", "WarehouseMovement-In");
            }else {
                CargoMessage.setAttribute("type", "WarehouseMovement-Out");
            }
            Document doc = new Document(CargoMessage);
            
            // HEADER
            Element Header = new Element("Header");
            Header.setAttribute("from", "RAPIDCARGO");
            Header.setAttribute("to", "CARGOINFO");
            Header.setAttribute("messageTime", new Date().toString());
            Header.setAttribute("messageId", UUID.randomUUID().toString());
            doc.getRootElement().addContent(Header);

            // WarehouseMovement
            Element movementTime = new Element("movementTime");
            movementTime.setText(movementDTO.getDate_movement().toString());

            Element declaredIn = new Element("declaredIn");
            declaredIn.setAttribute("code", "CDGRC1");
            declaredIn.setAttribute("label", "RapidCargo CDG");

            Element fromTo = new Element("fromTo");
            if(movementDTO instanceof EntryMovementDTO) {
                fromTo.setName("from");
            }else {
                fromTo.setName("to");
            }
            fromTo.setAttribute("code", movementDTO.getWarehouseCode());
            fromTo.setAttribute("label", movementDTO.getWarehouseName());

            // ------GOODS------
            Element ref = new Element("ref");
            ref.setAttribute("type", movementDTO.getRefType());
            ref.setAttribute("code", movementDTO.getReference().toString());

            Element amount = new Element("amount");
            amount.setAttribute("quantity", movementDTO.getQuantity().toString());
            amount.setAttribute("weight", movementDTO.getWeight().toString());

            Element description = new Element("description");
            description.setText(movementDTO.getMerchandiseDescription());

            Element totalRefAmount = new Element("retotalRefAmountf");
            totalRefAmount.setAttribute("quantity", movementDTO.getTotalQuantity().toString());
            totalRefAmount.setAttribute("weight", movementDTO.getTotalWeight().toString());

            Element goods = new Element("goods");
            goods.addContent(ref);
            goods.addContent(amount);
            goods.addContent(description);
            goods.addContent(totalRefAmount);
            // ------END GOODS------
            Element customsStatus = new Element("customsStatus");
            customsStatus.setText(movementDTO.getOfficerType());

            Element WarehouseMovement = new Element("WarehouseMovement");
            if(movementDTO instanceof EntryMovementDTO) {
                WarehouseMovement.setName("WarehouseMovementIn");
            }else {
                WarehouseMovement.setName("WarehouseMovementOut");
            }
            WarehouseMovement.addContent(movementTime);
            WarehouseMovement.addContent(declaredIn);
            WarehouseMovement.addContent(fromTo);
            WarehouseMovement.addContent(goods);
            WarehouseMovement.addContent(customsStatus);
            if(movementDTO instanceof ExitMovementDTO) { // INFO SUR DOCUMENT
                ExitMovementDTO exitMovementDTO = (ExitMovementDTO) movementDTO;
                Element customsDocument = new Element("customsDocument");
                customsDocument.setAttribute("type", exitMovementDTO.getDocumentType());
                customsDocument.setAttribute("ref", exitMovementDTO.getDocumentReference().toString());
                WarehouseMovement.addContent(customsDocument);
            };
            doc.getRootElement().addContent(WarehouseMovement);
            //// CREATE FILE
            XMLOutputter xml = new XMLOutputter();
            xml.setFormat(Format.getPrettyFormat());
            xml.output(doc, new FileWriter(this.pathFileXML));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}