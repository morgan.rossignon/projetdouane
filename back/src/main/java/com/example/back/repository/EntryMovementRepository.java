package com.example.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.back.model.EntryMovement;

@Repository
public interface EntryMovementRepository extends JpaRepository<EntryMovement, Long> {

}
