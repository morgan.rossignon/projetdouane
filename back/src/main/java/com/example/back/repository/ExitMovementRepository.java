package com.example.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.back.model.ExitMovement;

@Repository
public interface ExitMovementRepository extends JpaRepository<ExitMovement, Long>{
    
}
