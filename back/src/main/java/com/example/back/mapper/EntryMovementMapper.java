package com.example.back.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.example.back.model.EntryMovement;
import com.example.back.service.dto.EntryMovementDTO;

@Mapper
public interface EntryMovementMapper {

    EntryMovement map(EntryMovementDTO entryMovementDto);

    EntryMovementDTO map(EntryMovement entryMovement);

    List<EntryMovement> mapListFromDTO(List<EntryMovementDTO> entryMovementDtos);

    List<EntryMovementDTO> mapListToDTO(List<EntryMovement> entryMovements);
}