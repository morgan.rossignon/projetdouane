package com.example.back.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.example.back.model.ExitMovement;
import com.example.back.service.dto.ExitMovementDTO;

@Mapper
public interface ExitMovementMapper {
    
    ExitMovement map(ExitMovementDTO exitMovementDto);

    ExitMovementDTO map(ExitMovement exitMovement);

    List<ExitMovement> mapListFromDTO(List<ExitMovementDTO> exitMovementDtos);

    List<ExitMovementDTO> mapListToDTO(List<ExitMovement> exitMovements);
}
