package com.example.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.back.service.ExitMovementService;
import com.example.back.service.dto.ExitMovementDTO;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/exit-movement")
public class ExitMovementController {

    private ExitMovementService exitMovementService;

    @Autowired
    public ExitMovementController(ExitMovementService exitMovementService) {
        this.exitMovementService = exitMovementService;
    }
    
    @GetMapping(value = "/get-all")
    public List<ExitMovementDTO> getExitMovements() {
        return exitMovementService.findAll();
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Map<String, String>> createExitMovement(@RequestBody ExitMovementDTO exitMovementDTO) {
        try {
            exitMovementService.save(exitMovementDTO);
            Map<String, String> response = new HashMap<>();
            response.put("message", "Sortie créée avec succès.");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", e.getMessage());
            System.out.println(e.getMessage());
            return ResponseEntity.badRequest().body(errorResponse);
        }
    }

}
