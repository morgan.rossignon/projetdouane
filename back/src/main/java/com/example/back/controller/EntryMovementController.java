package com.example.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import com.example.back.service.EntryMovementService;
import com.example.back.service.dto.EntryMovementDTO;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/entry-movement")
@Component
public class EntryMovementController {
    
    private EntryMovementService entryMovementService;

    @Autowired
    public EntryMovementController(EntryMovementService entryMovementService) {
        this.entryMovementService = entryMovementService;
    }

    @GetMapping(value = "/get-all")
    public List<EntryMovementDTO> getEntryMovements() {
        return entryMovementService.findAll();
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Map<String, String>> createEntryMovement(@RequestBody EntryMovementDTO entryMovementDTO) {
        try {
            entryMovementService.save(entryMovementDTO);
            Map<String, String> response = new HashMap<>();
            response.put("message", "Entrée créée avec succès.");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", e.getMessage());
            return ResponseEntity.badRequest().body(errorResponse);
        }
    }
}
