CREATE DATABASE IF NOT EXISTS douanebdd;

USE douanebdd;

CREATE TABLE IF NOT EXISTS entry_movement (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_create VARCHAR(255),
    date_movement DATETIME,
    warehouse_Name VARCHAR(255),
    warehouse_Code VARCHAR(255),
    officer_Type VARCHAR(255),
    ref_Type VARCHAR(255),
    reference BIGINT,
    quantity INT,
    weight INT,
    total_Quantity INT,
    total_Weight INT,
    merchandise_Description TEXT
);

CREATE TABLE IF NOT EXISTS exit_movement (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_create VARCHAR(255),
    date_movement DATETIME,
    warehouse_Name VARCHAR(255),
    warehouse_Code VARCHAR(255),
    officer_Type VARCHAR(255),
    ref_Type VARCHAR(255),
    reference BIGINT,
    quantity INT,
    weight INT,
    total_Quantity INT,
    total_Weight INT,
    merchandise_Description TEXT,
    document_Type VARCHAR(255),
    document_Reference BIGINT
);