import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EntryMovementService } from '../services/EntryMovementService.service';
import { ListMovementsComponent } from '../list-movements/list-movements.component';

@Component({
  selector: 'app-entry-movement',
  templateUrl: './entry-movement.component.html',
  styleUrls: ['./entry-movement.component.scss']
})
export class EntryMovementComponent {
  warehouses = [
    "RAPIDCARGO"
  ];
  officerTypes = [
    "T1",
    "T2",
    "T3"
  ];
  refTypes = [
    'AWB'
  ];
  entryForm: FormGroup;
  errorMessage!: string;

  constructor(private formBuilder: FormBuilder, private entryMovementService: EntryMovementService, private listMovementsComponent: ListMovementsComponent) {
    this.entryForm = this.formBuilder.group({
      warehouseCode: ['', Validators.required],
      warehouseName: ['', Validators.required],
      officerType: ['', Validators.required],
      refType: ['', Validators.required],
      reference: ['', Validators.required],
      merchandiseDescription: ['', Validators.required],
      quantity: ['', Validators.required],
      weight:['', Validators.required],
      totalQuantity:['', Validators.required],
      totalWeight:['', Validators.required]
    });
  }

  onSubmit() {
    this.entryMovementService.postEntryMovement(this.entryForm.value).subscribe(
      (response) => {
        console.log(response);
        if (response) {
          console.log('Réponse du backend : ', response);
        } else {
          console.error('La réponse du backend est vide ou nulle.');
        }
        this.errorMessage = '';
      },
      (error) => {
        console.log(error);
        console.error('La requête a échoué : ', error.error);
        this.errorMessage = error.error.error;
      }
    );
  }

  cancelButton() {
    this.entryForm.reset();
  }

  onKeyPressOnlyNumber(event: KeyboardEvent) {
    const input = event.key;
    if (isNaN(Number(input))) {
      event.preventDefault();
    }
  }
}
