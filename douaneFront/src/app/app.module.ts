import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EntryMovementComponent } from './entry-movement/entry-movement.component';
import { ExitMovementComponent } from './exit-movement/exit-movement.component';
import { ListMovementsComponent } from './list-movements/list-movements.component';
import { MatTableModule } from '@angular/material/table' 
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    EntryMovementComponent,
    ExitMovementComponent,
    ListMovementsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTableModule
  ],
  providers: [
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
