import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EntryMovement } from '../models/EntryMovement.model';

@Injectable({
  providedIn: 'root'
})
export class EntryMovementService {

    private HTTPBaseUrl = 'http://localhost:8080/api/entry-movement';

    constructor(private http: HttpClient) { }
  
    getAllMovements(): Observable<EntryMovement[]> {
        return this.http.get<EntryMovement[]>(this.HTTPBaseUrl + '/get-all');
    }

    postEntryMovement(entryMovement: EntryMovement): Observable<any> {
        const res = this.http.post<any>(this.HTTPBaseUrl + '/create', entryMovement);
        console.log(res);
        return res;
    }
    
}