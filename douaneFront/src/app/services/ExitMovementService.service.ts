import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ExitMovement } from '../models/ExitMovement.model';

@Injectable({
    providedIn: 'root'
})
export class ExitMovementService {

    private HTTPBaseUrl = 'http://localhost:8080/api/exit-movement';

    constructor(private http: HttpClient) { }

    getAllMovements(): Observable<ExitMovement[]> {
        return this.http.get<ExitMovement[]>(this.HTTPBaseUrl + '/get-all');
    }

    postExitMovement(exitMovement: ExitMovement): Observable<any> {
        return this.http.post<any>(this.HTTPBaseUrl + '/create', exitMovement);
    }
}