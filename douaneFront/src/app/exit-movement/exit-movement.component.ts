import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExitMovementService } from '../services/ExitMovementService.service';

@Component({
  selector: 'app-exit-movement',
  templateUrl: './exit-movement.component.html',
  styleUrls: ['./exit-movement.component.scss']
})
export class ExitMovementComponent {
  warehouses = [
    "RAPIDCARGO"
  ];
  officerTypes = [
    "T1",
    "T2",
    "T3"
  ];
  refTypes = [
    'AWB'
  ];
  exitForm: FormGroup;
  errorMessage!: string;

  constructor(private formBuilder: FormBuilder, private exitMovementService: ExitMovementService) {
    this.exitForm = this.formBuilder.group({
      warehouseCode: ['', Validators.required],
      warehouseName: ['', Validators.required],
      officerType: ['', Validators.required],
      refType: ['', Validators.required],
      reference: ['', Validators.required],
      documentType: ['', Validators.required],
      documentReference: ['', Validators.required],
      merchandiseDescription: ['', Validators.required],
      quantity: ['', Validators.required],
      weight:['', Validators.required],
      totalQuantity:['', Validators.required],
      totalWeight:['', Validators.required]
    });
  }

  onSubmit() {
    this.exitMovementService.postExitMovement(this.exitForm.value).subscribe(
      (response) => {
        if (response) {
          console.log('Réponse du backend : ', response);
        } else {
          console.error('La réponse du backend est vide ou nulle.');
        }
        this.errorMessage = '';
      },
      (error) => {
        console.error('La requête a échoué : ', error.error);
        this.errorMessage = error.error.error;
      }
    );
  }

  cancelButton() {
    this.exitForm.reset();
  }

  onKeyPressOnlyNumber(event: KeyboardEvent) {
    const input = event.key;
    if (isNaN(Number(input))) {
      event.preventDefault();
    }
  }
}
