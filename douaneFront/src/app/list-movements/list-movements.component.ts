import { Component, ViewChild } from '@angular/core';
import { EntryMovementService } from '../services/EntryMovementService.service';
import { ExitMovementService } from '../services/ExitMovementService.service';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';

export interface displayedMovement {
  horodatage: string;
  type: string;
  warehouse: string;
  reference: number;
  quantity: number;
  weight: number;
  officerType: string;
}

@Component({
  selector: 'app-list-movements',
  templateUrl: './list-movements.component.html',
  styleUrls: ['./list-movements.component.scss']
})
@Injectable({
  providedIn: 'root',
})
export class ListMovementsComponent {
  displayedColumns: string[] = ['horodatage', 'type', 'warehouse', 'reference', 'quantity', 'weight', 'officerType']
  allMovements: displayedMovement[] = [];
  dataSource = new MatTableDataSource<displayedMovement>(this.allMovements);
  @ViewChild(MatTable) table!: MatTable<displayedMovement>;

  constructor( private entryMovementService: EntryMovementService, private exitMovementService: ExitMovementService, public datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.updateAllMovements();
  }

  async updateAllMovements() {
    this.allMovements = []
    try {
      const entryMovements = await this.entryMovementService.getAllMovements().toPromise();
      entryMovements?.forEach( movement => {
        this.allMovements.push({
          horodatage: this.datePipe.transform(movement.date_movement, 'dd/MM/yyyy hh:mm:ss')!,
          type: 'Entrée',
          warehouse: movement.warehouseName,
          reference: movement.reference,
          quantity: movement.quantity,
          weight: movement.weight,
          officerType: movement.officerType
        })
      });
      const exitMovements = await this.exitMovementService.getAllMovements().toPromise()
      exitMovements?.forEach( movement => {
        this.allMovements.push({
          horodatage: this.datePipe.transform(movement.date_movement, 'dd/MM/yyyy hh:mm:ss')!,
          type: 'Sortie',
          warehouse: movement.warehouseName,
          reference: movement.reference,
          quantity: movement.quantity,
          weight: movement.weight,
          officerType: movement.officerType
        })
      });
      this.allMovements.sort((a, b) => new Date(b.horodatage).getTime() - new Date(a.horodatage).getTime());
      this.dataSource.data = this.allMovements.slice(0,50);
      this.table.renderRows();
    } catch (error) {
      console.error(error);
    }
  }
}
