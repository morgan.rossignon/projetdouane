export class EntryMovement {
    id! : number;
    user_create! : string;
    date_movement! : Date;
    warehouseName! : string;
    warehouseCode! : string;
    officerType! : string;
    refType! : string;
    reference! : number;
    quantity! : number;
    weight! : number;
    totalQuantity! : number;
    totalWeight! : number;
    merchandiseDescription! : string;
}
